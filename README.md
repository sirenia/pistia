# PISTIA

```



           dBBBBBb  dBP.dBBBBP  dBBBBBBP dBP dBBBBBb
               dB'     BP                         BB
           dBBBP' dBP  'BBBBb    dBP   dBP    dBP BB
          dBP    dBP      dBP   dBP   dBP    dBP  BB
         dBP    dBP  dBBBBP'   dBP   dBP    dBBBBBBB
         1.0.0

Usage:
  pistia run [flags]

Flags:
      --aripuana-flush-interval duration     Interval between flushes to aripuana (default 30s)
      --aripuana-url string                  Aripuana url
      --debounce-update-interval duration    Interval between status updates to kwanza (default 10s)
      --debug                                Enable debug interfaces (expvars and pprof)
      --heartbeat duration                   Interval between connected-and-alive heartbeats to kwanza (default 2m0s)
  -h, --help                                 help for run
      --install                              Install pistia as a service
      --kwanza-reconnect-interval duration   Interval between reconnect attempts to kwanza (default 5s)
      --machine string                       Name of machine to use when selecting which desktops to connect to. Default is current machine name (if left blank)
      --machine-only                         Only maintain connections to desktops on the same machine as pistia is running
      --password string                      Kwanza password
      --rdp-domain string                    Default RDP domain to use when no domain is specified in the login
      --rdp-port int                         Default RDP port to use when no port is specified in the login (default 3389)
      --reconnect-interval duration          Interval between reconnect attempts to RDP servers (default 1m0s)
      --token string                         Kwanza token
      --uninstall                            Uninstall the pistia service
      --url string                           Kwanza url
      --user string                          Kwanza username

Global Flags:
      --log-level string   Log level (debug, info, warn, error, panic, fatal) (default "info")
      --verbose            Enable verbose output (debug)
```

Pistia is a tool that connects to Kwanza and maintains RDP connections to desktops. It also sends logs to Aripuana if so configured.

Normally you would run Pistia as a service on a machine that has access to the desktops you want to connect to. Pistia will then maintain connections to the desktops and keep Kwanza updated with the status of the desktops.

## Requirements

- Access to create RDP connections to desktops.
- Windows 10 or Windows Server 2016 or later, OSX or Linux.
- Kwanza v3+
- Aripuana v1+ (optional for logs)

## Installing PISTIA as a service

You can run the `run` command with the `--install` flag to install PISTIA as a service. You should provide the `--url`, `--token` or `--user` and `--password` flags to connect to Kwanza and `--machine-only` to only use logins for this machine. E.g.:

```
pistia run --install --url kwanza.sirenia.io --token 038n09fsd0lsdkfjlskjf --machine-only
```

That will install PISTIA as a service, and you can then start and stop it using the Windows Services Manager. You can uninstall the service using the `--uninstall` flag.

### Logging

You should consider using the `--aripuana-url` flag to send logs to Aripuana. That makes error logs available to help diagnose and debug issues issues.

### Debugging

For debugging purposes, you can use the `--debug` flag to expose the expvars and pprof interfaces. You can also use the `--log-lebel` flag to set the log level. The default is `info`.

The `--verbose` flag will enable debug output to stdout.

### Standard Deployment

In an admin Power Shell

```
mkdir 'C:\Program Files (x86)\Sirenia\Pistia'
cd 'C:\Program Files (x86)\Sirenia\Pistia'

Invoke-WebRequest https://releases.sirenia.io/pistia/pistia-v1.0.21.exe -OutFile pistia-v1.0.21.exe
cmd /c .\pistia-v1.0.21.exe run  --url <host>.sirenia.cloud:8001 --user pistia --password XXX --machine-only --debounce-update-interval 2s --aripuana-url <host>.sirenia.cloud:8089 --debug --install

service eu.sirenia.pistia
services.msc
```
Find PISTIA - Sirenia RDP Manager in the service panel. Open Properties. Select Recovery tab. Select Restart the Service on all three drop downs Firstfailure, Second failure, and Subsequent failures. Press Apply and OK

```
net start eu.sirenia.pistia
service eu.sirenia.pistia
```

---

## For developers

Needs a fix: https://github.com/tomatome/grdp/issues/13

### Requirements for Windows

1. golang, gcc, protoc: `choco install -y mingw protoc golang`
2. go tooling:

- `go install golang.org/x/tools/cmd/goimports@latest`
- `go install github.com/mna/pigeon@latest`
- `go install google.golang.org/protobuf/cmd/protoc-gen-go@latest`
- `go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest`

3. now you can run `go generate ./...` to generate protocol buffers and parsers
4. and then `go build ./...` to build the project

```

```
