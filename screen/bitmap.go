package screen

import (
	"github.com/tomatome/grdp/core"
	"github.com/tomatome/grdp/glog"
	"github.com/tomatome/grdp/protocol/pdu"
	"go.uber.org/zap"
)

func Bpp(BitsPerPixel uint16) (pixel int) {
	switch BitsPerPixel {
	case 15:
		pixel = 1

	case 16:
		pixel = 2

	case 24:
		pixel = 3

	case 32:
		pixel = 4

	default:
		glog.Error("invalid bitmap data format")
	}
	return
}

func BitmapDecompress(bitmap *pdu.BitmapData) []byte {
	return core.Decompress(bitmap.BitmapDataStream, int(bitmap.Width), int(bitmap.Height), Bpp(bitmap.BitsPerPixel))
}

func ToRGBA(pixel int, i int, data []byte) (r, g, b, a uint8) {
	a = 255
	switch pixel {
	case 1:
		rgb555 := core.Uint16BE(data[i], data[i+1])
		r, g, b = core.RGB555ToRGB(rgb555)
	case 2:
		rgb565 := core.Uint16BE(data[i], data[i+1])
		r, g, b = core.RGB565ToRGB(rgb565)
	case 3, 4:
		fallthrough
	default:
		r, g, b = data[i+2], data[i+1], data[i]
	}

	return
}

type Bitmap struct {
	Data         []byte `json:"data"`
	DestLeft     int    `json:"destLeft"`
	DestTop      int    `json:"destTop"`
	DestRight    int    `json:"destRight"`
	DestBottom   int    `json:"destBottom"`
	Width        int    `json:"width"`
	Height       int    `json:"height"`
	BitsPerPixel int    `json:"bitsPerPixel"`
	IsCompress   bool   `json:"isCompress"`
}

type Screen struct {
	Width  int
	Height int
}

// TODO Patch will insert the given rects/images into the current screen representation
func (s *Screen) Patch(rects []pdu.BitmapData) {
	zap.L().Debug("[pdf] bitmaps")

	/*bs := make([]Bitmap, 0, 50)
	for _, v := range rects {
		IsCompress := v.IsCompress()
		data := v.BitmapDataStream
		if IsCompress {
			data = BitmapDecompress(&v)
			IsCompress = false
		}

		b := Bitmap{
			int(v.DestLeft), int(v.DestTop), int(v.DestRight), int(v.DestBottom),
			int(v.Width), int(v.Height), Bpp(v.BitsPerPixel), IsCompress, data,
		}
		bs = append(bs, b)
	}

	var (
		pixel      int
		i          int
		r, g, b, a uint8
	)

	count := 0
	for _, bm := range bs {
		i = 0
		pixel = bm.BitsPerPixel
		m := image.NewRGBA(image.Rect(0, 0, bm.Width, bm.Height))
		for y := 0; y < bm.Height; y++ {
			for x := 0; x < bm.Width; x++ {
				r, g, b, a = ToRGBA(pixel, i, bm.Data)
				c := color.RGBA{r, g, b, a}
				i += pixel
				m.Set(x, y, c)
			}
		}

		// TODO should patch internal screen representation
		zap.L().Debug("Writing image", zap.Int("i", i))

		f, err := os.Create(fmt.Sprintf("img-%d.jpg", count))
		if err != nil {
			panic(err)
		}
		defer f.Close()
		if err = jpeg.Encode(f, m, nil); err != nil {
			log.Printf("failed to encode: %v", err)
		}
		count += 1

		// os.WriteFile(fmt.Sprintf("out-%d.bmp"), m. , 0666)
	}
	*/
}
