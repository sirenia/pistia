package kwanza

import (
	"encoding/json"
	"errors"
	"fmt"
)

var (
	errReconnect    = errors.New("reconnect")
	DesktopTypeName = "desktop"
)

type Identifiable interface {
	Identifier() string
}

type Named interface {
	Name() string
}

type Described interface {
	Description() (string, error)
}

type Versioned interface {
	CurrentVersion() string
}

type Updateable interface {
	Update(id, version string)
}

type Desktop struct {
	Descriptive *DesktopDescriptive `json:"descriptive"`
	Id          string              `json:"id"`
	Name        string              `json:"name"`
	Version     string              `json:"version"`
}

func (d *Desktop) String() string {
	return fmt.Sprintf("Desktop{Id: %s, Name: %s, Version: %s, Descriptive: %s}", d.Id, d.Name, d.Version, d.Descriptive)
}

type DesktopDescriptive struct {
	Host        string   `json:"host"`
	Address     string   `json:"address"`
	ConnectWith string   `json:"connectWith"`
	Machine     string   `json:"machine"`
	Logins      []*Login `json:"logins"`
	Port        *int     `json:"port"`
}

// Key returns a unique key for the Desktop. It is used to determine if the connection should be removed or recreated.
func (d *Desktop) Key() string {
	identifier := d.Identifier()
	host := ""
	port := 0
	machine := ""
	connectWith := ""
	if d.Descriptive != nil {
		host = d.Descriptive.Host
		machine = d.Descriptive.Machine
		connectWith = d.Descriptive.ConnectWith
		if d.Descriptive.Port != nil {
			port = *d.Descriptive.Port
		}
	}
	return fmt.Sprintf("Desktop{Descriptive:{Id: %s, Host: %s, Port: %d, Machine: %s, ConnectWith: %s}}", identifier, host, port, machine, connectWith)
}

func (d *DesktopDescriptive) String() string {
	return fmt.Sprintf("DesktopDescriptive{Host: %s, Address: %s, ConnectWith: %s, Machine: %s, Port: %d}", d.Host, d.Address, d.ConnectWith, d.Machine, d.Port)
}

type Login struct {
	Id             string           `json:"id"`
	UserPlaintext  string           `json:"usernamePlaintext"`
	UserSecret     *SecretReference `json:"username"`
	PasswordSecret *SecretReference `json:"password"`
	Domain         string           `json:"domain"`
	Host           string           `json:"host"`
	Port           *int             `json:"port"`
	Disabled       bool             `json:"disabled"`
}

// Key returns a unique key for the Login. It is used to determine if the connection should be removed or recreated.
func (l *Login) Key() string {
	return fmt.Sprintf("Login{Domain: %s, UserPlaintext: %s,  UserSecret: %s, PasswordSecret: %s, Host: %s, Port: %d, Disabled: %v}", l.Domain, l.UserPlaintext, l.UserSecret, l.PasswordSecret, l.Host, l.Port, l.Disabled)
}

func (l *Login) String() string {
	return fmt.Sprintf("Login{Id %s, Domain: %s, UserPlaintext: %s,  UserSecret: %s, PasswordSecret: %s, Host: %s, Port: %d}", l.Id, l.Domain, l.UserPlaintext, l.UserSecret, l.PasswordSecret, l.Host, l.Port)
}

func (d *Desktop) Identifier() string {
	return d.Id
}

type SecretReference struct {
	Secret   string `json:"secret"`
	Property string `json:"property"`
}

func (s *SecretReference) Equals(other *SecretReference) bool {
	return s.Secret == other.Secret && s.Property == other.Property
}

func (s *SecretReference) String() string {
	return fmt.Sprintf("SecretReference{Secret: %s, Property: %s}", s.Secret, s.Property)
}

func DesktopFactory(sr *StoredRegisterable) (*Desktop, error) {
	d := &Desktop{Id: sr.Identifier, Name: sr.InstanceName, Version: sr.Version}
	dd := &DesktopDescriptive{}
	err := json.Unmarshal([]byte(sr.DescriptiveData), dd)
	if err != nil {
		return nil, err
	}
	d.Descriptive = dd

	return d, nil
}
