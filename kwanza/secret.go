package kwanza

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"time"

	"gitlab.com/sirenia/pistia/encryption/aes"
	"gitlab.com/sirenia/pistia/encryption/machine"
	"go.uber.org/zap"
)

var (
	errUnsupportedEncryptionType = errors.New("unsupported encryption type")
	errMissingProperty           = errors.New("missing property")
	errSecretExpired             = errors.New("secret expired")
	SecretTypeName               = "secret"
)

type Secret struct {
	Descriptive *SecretDescriptive `json:"descriptive"`
	Id          string             `json:"id"`
	Name        string             `json:"name"`
	Version     string             `json:"version"`
}

type SecretDescriptive struct {
	Encrypted  map[string]string `json:"encrypted"`
	Type       string            `json:"type"`
	ValidUntil int64             `json:"validUntil"`
}

func (s *Secret) Identifier() string {
	return s.Id
}

func globalDecrypt(cipherText string) (string, error) {
	cipherBytes, err := base64.StdEncoding.DecodeString(cipherText)
	if err != nil {
		zap.L().Warn("Failed to base64 decode ciphertext", zap.Error(err))
		return "", err
	}

	plainText, err := aes.Decrypt(cipherBytes, aes.ChooseKey(""))
	if err != nil {
		zap.L().Warn("Failed to decrypt ciphertext", zap.Error(err))
		return "", err
	}

	return string(plainText), nil
}

func machineDecrypt(cipherText string) (string, error) {
	cipherBytes, err := base64.StdEncoding.DecodeString(cipherText)
	if err != nil {
		zap.L().Warn("Failed to base64 decode ciphertext", zap.Error(err))
		return "", err
	}
	plainText, err := machine.MachineDecrypt(string(cipherBytes))
	if err != nil {
		zap.L().Warn("Failed to decrypt ciphertext", zap.Error(err))
		return "", err
	}
	return string(plainText), nil
}

func (s *Secret) DecryptProperty(name string) (string, error) {
	if s.Descriptive == nil {
		return "", errMissingProperty
	}

	// Check if secret is expired
	if s.Descriptive.ValidUntil > 0 && time.Now().After(time.Unix(s.Descriptive.ValidUntil, 0)) {
		return "", errSecretExpired
	}

	v, ok := s.Descriptive.Encrypted[name]
	if !ok {
		return "", errMissingProperty
	}

	switch s.Descriptive.Type {
	case "global":
		return globalDecrypt(v)
	case "machine":
		return machineDecrypt(v)
	default:
		zap.L().Warn("Unsupported encryption type, needs 'global' or 'machine'", zap.String("encryption", s.Descriptive.Type))
		return "", errUnsupportedEncryptionType

	}
}

func SecretFactory(sr *StoredRegisterable) (*Secret, error) {
	ds := &Secret{Id: sr.Identifier, Name: sr.InstanceName, Version: sr.Version}
	d := &SecretDescriptive{}
	err := json.Unmarshal([]byte(sr.DescriptiveData), d)
	if err != nil {
		return nil, err
	}
	ds.Descriptive = d

	return ds, nil
}
