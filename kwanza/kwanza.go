package kwanza

import (
	"context"
	"crypto/tls"
	"fmt"
	"io"
	"math"
	sync "sync"
	"time"

	"github.com/spf13/viper"
	"gitlab.com/sirenia/pistia/flags"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/status"
)

//go:generate protoc --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative kwanza.proto

type jwt struct {
	token string
}

// GetRequestMetadata returns "authorization" header.
func (j jwt) GetRequestMetadata(ctx context.Context, uri ...string) (map[string]string, error) {
	return map[string]string{
		"authorization": j.token,
	}, nil
}

// RequireTransportSecurity yes.
func (j jwt) RequireTransportSecurity() bool {
	return true
}

// Connect to Kwanza!
func connect() (*grpc.ClientConn, error) {
	// TODO: reuse connections
	zap.L().Debug("Connecting", zap.String("kwanzaUrl", viper.GetString(flags.KwanzaUrl)))

	//nolint:gosec
	creds := credentials.NewTLS(&tls.Config{InsecureSkipVerify: true}) // nosemgrep - fine to connect to non-https kwanza

	var conn *grpc.ClientConn
	var err error
	remote := viper.GetString(flags.KwanzaUrl)
	token := viper.GetString(flags.KwanzaToken)

	if token == "" {
		conn, err = grpc.NewClient(
			remote,
			grpc.WithTransportCredentials(creds),
			grpc.WithDefaultCallOptions(
				grpc.MaxCallRecvMsgSize(math.MaxInt32),
				grpc.MaxCallSendMsgSize(math.MaxInt32),
			),
		)
		if err != nil {
			return nil, err
		}

		user := viper.GetString(flags.KwanzaUser)
		pass := viper.GetString(flags.KwanzaPassword)
		if user != "" && pass != "" {
			zap.L().Debug("Authenticating", zap.String("user", user))
			token, err = authenticate(conn, user, pass)
			if err != nil {
				conn.Close()
				zap.L().Debug("Failed to authenticate", zap.Error(err))
				return nil, err
			}
		}
	}

	if token != "" {
		// Read token
		jwtoken := newTokenFromString(token)
		conn, err = grpc.NewClient(
			remote,
			grpc.WithTransportCredentials(creds),
			grpc.WithPerRPCCredentials(jwtoken),
			grpc.WithDefaultCallOptions(
				grpc.MaxCallRecvMsgSize(math.MaxInt32),
				grpc.MaxCallSendMsgSize(math.MaxInt32),
			))
		if err != nil {
			return nil, err
		}
	}
	return conn, nil
}

func authenticate(cc *grpc.ClientConn, user, pass string) (string, error) {
	c := NewAuthenticatorClient(cc)
	result, err := c.Authenticate(context.Background(), &AuthenticateRequest{Username: user, Password: pass})
	if err != nil {
		zap.L().Error("Could not authenticate", zap.Error(err), zap.Error(err), zap.String("kwanzaUrl", viper.GetString(flags.KwanzaUrl)))
		return "", err
	}

	return result.Token, nil
}

// NewTokenFromString reads token from a string and returns it.
func newTokenFromString(token string) credentials.PerRPCCredentials {
	return jwt{token}
}

type Providee interface {
	Identifiable
	Named
	Described
	Versioned
	Updateable
	fmt.Stringer
}

func provide[T Providee](ctx context.Context, client ProviderClient, typeName string, item T) error {
	description, err := item.Description()
	if err != nil {
		return err
	}
	provided, err := client.Provide(ctx, &ProvideRequest{
		TypeName:   typeName,
		Identifier: item.Identifier(),
		Item: &Registerable{
			InstanceName:    item.Name(),
			Version:         item.CurrentVersion(),
			DescriptiveData: description,
		},
	})
	if err != nil {
		return err
	}

	// Update item
	item.Update(provided.Identifier, provided.Version)

	return err
}

func fetch[T Identifiable](ctx context.Context, client InquirerClient, typeName, kql string, f factory[T]) ([]T, error) {
	zap.L().Debug("Fetching", zap.String("typeName", typeName), zap.String("kql", kql))
	results, err := client.QueryDescriptive(ctx, &QueryDescriptiveRequest{
		TypeName: typeName,
		Kql:      kql,
	})
	if err != nil {
		return nil, err
	}

	items := []T{}
	for _, result := range results.Results {
		item, err := f(result)
		if err != nil {
			return nil, err
		}
		items = append(items, item)
	}

	return items, nil
}

type factory[T Identifiable] func(sr *StoredRegisterable) (T, error)

func stream[T Identifiable](ctx context.Context, sc StreamerClient, typeName, kql string, items []T, f factory[T], out chan<- []T) error {
	c, err := sc.Stream(ctx, &StreamRequest{
		TypeName: typeName,
		Kql:      kql,
	})
	if err != nil {
		zap.L().Warn("Stream init failed", zap.Error(err), zap.String("typeName", typeName), zap.String("kql", kql))
		return errReconnect
	}

	for {
		result, err := c.Recv()
		if err == context.Canceled {
			zap.L().Warn("Context cancelled - no reconnect")
			return err
		}
		if err == io.EOF {
			zap.L().Warn("Stream EOF - no reconnect")
			return errReconnect
		}
		if err != nil {
			// Unwrap grpc error
			if s, ok := status.FromError(err); ok {
				switch s.Code() {
				case codes.Canceled:
					zap.L().Warn("Context cancelled - reconnect", zap.Stringer("status", s), zap.Error(err), zap.String("typeName", typeName), zap.String("kql", kql))
					return errReconnect
				case codes.PermissionDenied, codes.Unauthenticated:
					zap.L().Warn("Authentication error - reconnect", zap.Stringer("status", s), zap.Error(err), zap.String("typeName", typeName), zap.String("kql", kql))
					return errReconnect
				case codes.Unknown:
					zap.L().Warn("Unknown connection error - reconnect", zap.Stringer("status", s), zap.Error(err), zap.String("typeName", typeName), zap.String("kql", kql))
					return errReconnect
				default:
					zap.L().Warn("Streaming item failed (grpc error) - reconnect", zap.Error(err), zap.String("typeName", typeName), zap.String("kql", kql), zap.Stringer("status", s))
					return errReconnect
				}
			}

			zap.L().Warn("Streaming item failed - reconnect", zap.Error(err))
			return errReconnect
		}

		items, anyChanges := merge(items, result, f)
		if !anyChanges {
			continue
		}

		zap.L().Debug("Merged items", zap.Int("count", len(items)))
		out <- items
	}
}

func merge[T Identifiable](existing []T, r *StreamResponse, f factory[T]) ([]T, bool) {
	items := make([]T, len(existing))
	copy(items, existing)

	if r.Old == nil && r.New == nil {
		// ping
		return nil, false
	} else if r.Old == nil && r.New != nil {
		// Addition
		newItem, err := f(r.New)
		if err != nil {
			zap.L().Warn("Failed to make item", zap.Error(err))
			return items, false
		}
		items = append(items, newItem)
	} else if r.Old != nil && r.New == nil {
		// Removal
		for i, item := range items {
			if item.Identifier() == r.Old.Identifier {
				items = append(items[:i], items[i+1:]...)
				break
			}
		}
	} else {
		// Update
		newItem, err := f(r.New)
		if err != nil {
			zap.L().Warn("Failed to make item", zap.Error(err))
		}
		found := false
		for i, item := range items {
			if item.Identifier() == newItem.Identifier() {
				items[i] = newItem
				zap.L().Debug("Updated item", zap.String("id", item.Identifier()))
				found = true
				break
			}
		}

		// If we havent found the thing, add it
		if !found {
			items = append(items, newItem)
		}
	}
	return items, true
}

func Sync[T Identifiable](ctx context.Context, typeName, kql string, f factory[T], out chan<- []T) {
	var conn *grpc.ClientConn
	var connErr error
	for {

		if conn != nil {
			conn.Close()
			time.Sleep(viper.GetDuration(flags.KwanzaReconnectInterval))
		}

		conn, connErr = connect()
		if connErr != nil {
			d := viper.GetDuration(flags.KwanzaReconnectInterval)
			zap.L().Warn("Failed to connect to server, retrying", zap.Error(connErr), zap.Duration("delay", d), zap.String("typeName", typeName), zap.String("kql", kql))
			time.Sleep(d)
			continue
		}

		client := NewInquirerClient(conn)
		streamClient := NewStreamerClient(conn)

		zap.L().Debug("Connected to Kwanza", zap.String("typeName", typeName), zap.String("kql", kql))

		items, err := fetch(ctx, client, typeName, kql, f)
		if err != nil {
			zap.L().Warn("Failed to fetch initial items", zap.Error(err), zap.String("typeName", typeName), zap.String("kql", kql))
			continue
		} else {
			zap.L().Debug("Fetched initial items", zap.Int("count", len(items)), zap.String("typeName", typeName), zap.String("kql", kql))
			out <- items
			// This will block until the stream is closed
			streamErr := stream(ctx, streamClient, typeName, kql, items, f, out)
			if streamErr == errReconnect {
				zap.L().Warn("Streaming error - reconnecting after 5 secs", zap.String("typeName", typeName), zap.String("kql", kql))
				continue
			}
			if streamErr != nil {
				zap.L().Warn("Streaming error - not reconnecting", zap.Error(streamErr), zap.String("typeName", typeName), zap.String("kql", kql))
				break
			}
			zap.L().Warn("Streaming shutting down normally. This should only happen when pistia shuts down.", zap.String("typeName", typeName), zap.String("kql", kql))
		}
	}
}

func Provide[T Providee](ctx context.Context, typeName string, in <-chan T) {
	var conn *grpc.ClientConn
	var connErr error
	for {

		if conn != nil {
			conn.Close()
			zap.L().Debug("Connection closed, waiting for reconnect interval", zap.String("typeName", typeName))
			time.Sleep(viper.GetDuration(flags.KwanzaReconnectInterval))
		}

		conn, connErr = connect()
		if connErr != nil {
			d := viper.GetDuration(flags.KwanzaReconnectInterval)
			zap.L().Warn("Failed to connect to server, retrying in a bit", zap.Error(connErr), zap.Duration("delay", d))
			time.Sleep(d)
			continue
		}

		client := NewProviderClient(conn)
		zap.L().Info("Connected to Kwanza", zap.String("typeName", typeName))

		for item := range in {
			zap.L().Debug(
				"About to provide item",
				zap.String("typeName", typeName),
				zap.Stringer("item", item),
			)
			err := provide(ctx, client, typeName, item)
			if err != nil {
				zap.L().Warn("Failed to provide item, reconnecting", zap.String("typeName", typeName), zap.Error(err))
				break
			}
		}

		zap.L().Info("Provide shutting down normally (chan closed)", zap.String("typeName", typeName))
	}
}

type Archivable interface {
	Identifiable
}

type Archive[K Archivable] struct {
	secrets map[string]K
	mutex   sync.RWMutex
}

func NewArchive[K Archivable]() *Archive[K] {
	return &Archive[K]{
		secrets: make(map[string]K),
		mutex:   sync.RWMutex{},
	}
}

func (a *Archive[K]) Update(items []K) {
	a.mutex.Lock()
	defer a.mutex.Unlock()

	for _, s := range items {
		a.secrets[s.Identifier()] = s
	}
}

func (a *Archive[K]) Add(s K) {
	a.mutex.Lock()
	defer a.mutex.Unlock()
	a.secrets[s.Identifier()] = s
}

func (a *Archive[K]) Get(id string) (K, bool) {
	a.mutex.RLock()
	defer a.mutex.RUnlock()
	s, ok := a.secrets[id]
	return s, ok
}

func (a *Archive[K]) Find(f func(K) bool) (K, bool) {
	a.mutex.RLock()
	defer a.mutex.RUnlock()
	for _, s := range a.secrets {
		if f(s) {
			return s, true
		}
	}
	var defaultItem K
	return defaultItem, false
}

func (a *Archive[K]) Delete(id string) {
	a.mutex.Lock()
	defer a.mutex.Unlock()
	delete(a.secrets, id)
}

func (a *Archive[K]) All() []K {
	a.mutex.RLock()
	defer a.mutex.RUnlock()
	secrets := make([]K, 0, len(a.secrets))
	for _, s := range a.secrets {
		secrets = append(secrets, s)
	}
	return secrets
}
