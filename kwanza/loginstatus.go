package kwanza

import (
	"encoding/json"
	"fmt"
)

var LoginStatusTypeName = "desktop_login_status"

type LoginStatus struct {
	Descriptive  *LoginStatusDescriptive `json:"descriptive"`
	Id           string                  `json:"id"`
	InstanceName string                  `json:"name"`
	Version      string                  `json:"version"`
}

func (ls *LoginStatus) Identifier() string {
	return ls.Id
}

func (ls *LoginStatus) Name() string {
	return ls.InstanceName
}

func (ls *LoginStatus) Description() (string, error) {
	// Return Descriptive as JSON
	jsonBytes, err := json.Marshal(ls.Descriptive)
	if err != nil {
		return "", err
	}

	return string(jsonBytes), nil
}

func (l *LoginStatus) Update(id, version string) {
	l.Id = id
	l.Version = version
}

func (l *LoginStatus) CurrentVersion() string {
	return l.Version
}

func (l *LoginStatus) String() string {
	return fmt.Sprintf(
		"LoginStatus{Id: %s, Name: %s, Version: %s, Descriptive: %s}",
		l.Id,
		l.InstanceName,
		l.Version,
		l.Descriptive,
	)
}

type LoginStatusDescriptive struct {
	For       string `json:"for"`
	Error     string `json:"error"`
	Connected bool   `json:"connected"`
	At        int64  `json:"at"`
}

func (lsd *LoginStatusDescriptive) String() string {
	return fmt.Sprintf(
		"LoginStatusDescriptive{For: %s, Error: %s, Connected: %t, At: %d}",
		lsd.For,
		lsd.Error,
		lsd.Connected,
		lsd.At,
	)
}

func LoginStatusFactory(sr *StoredRegisterable) (*LoginStatus, error) {
	ls := &LoginStatus{Id: sr.Identifier, InstanceName: sr.InstanceName, Version: sr.Version}
	lsd := &LoginStatusDescriptive{}
	err := json.Unmarshal([]byte(sr.DescriptiveData), lsd)
	if err != nil {
		return nil, err
	}
	ls.Descriptive = lsd

	return ls, nil
}
