package aes

import (
	"encoding/base64"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestEncryptDecrypt(t *testing.T) {
	key := NewEncryptionKey()
	plaintext := []byte("I am a very secret message")
	ciphertext, err := Encrypt(plaintext, key)
	assert.NoError(t, err)
	decrypted, err := Decrypt(ciphertext, key)
	assert.NoError(t, err)
	assert.Equal(t, plaintext, decrypted)
}

func TestDecryptFromManatee(t *testing.T) {
	cipher := "xdg9cs/a3M6ugUKk3ktZmnlFrYIfTmjy1q7SetaySlSZP7EoUs8v/zEDSpey67e/V0Dpn0bk"
	key := "secretkey"

	// base64 decode cipher
	cipherBytes, err := base64.StdEncoding.DecodeString(cipher)
	assert.NoError(t, err)

	plaintext, err := Decrypt(cipherBytes, ChooseKey(key))
	assert.NoError(t, err)

	assert.Equal(t, "I am a very secret message", string(plaintext))
}
