package machine

import (
	"errors"
	"runtime"
)

var errMachineDecryptionNotSupported = errors.New("machine decryption not supported on " + runtime.GOOS)

func MachineDecrypt(cipherText string) (string, error) {
	return "", errMachineDecryptionNotSupported
}
