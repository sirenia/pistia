package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/sirenia/pistia/flags"
	"go.uber.org/zap"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "pistia",
	Short: "pistia is a tool to maintain RDP connections",
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	// Setup zap logger
	var logger *zap.Logger
	if viper.GetBool(flags.Verbose) {
		logger, _ = zap.NewDevelopment()
	} else {
		logger, _ = zap.NewProduction()
	}
	defer func() { _ = logger.Sync() }()
	zap.ReplaceGlobals(logger)

	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	rootCmd.PersistentFlags().Bool(flags.Verbose, false, "Enable verbose output (debug)")
	rootCmd.PersistentFlags().String(flags.LogLevel, "info", "Log level (debug, info, warn, error, panic, fatal)")

	if err := viper.BindPFlags(rootCmd.Flags()); err != nil {
		fmt.Println("Could not bind flags.")
	}
}
