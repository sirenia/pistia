/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/sirenia/pistia/connection"
	"gitlab.com/sirenia/pistia/flags"
)

// connectCmd represents the connect command
var connectCmd = &cobra.Command{
	Use:   "connect",
	Short: "Connect to a single desktop",
	Run: func(_ *cobra.Command, _ []string) {
		user := viper.GetString(flags.RdpUser)
		password := viper.GetString(flags.RdpPassword)
		address := viper.GetString(flags.RdpAddress)
		port := viper.GetInt(flags.RdpPort)
		domain := viper.GetString(flags.RdpDomain)

		fmt.Println("Connecting to ", address, " on port ", port, " as ", user, " in domain ", domain)
		conn := connection.NewRdp(address, port, user, password, domain)
		err := conn.Connect()
		if err != nil {
			fmt.Println("Connection error: ", err)
		}
	},
}

func init() {
	rootCmd.AddCommand(connectCmd)

	connectCmd.Flags().String(flags.RdpUser, "", "Username for the RDP connection")
	connectCmd.Flags().String(flags.RdpPassword, "", "Password")
	connectCmd.Flags().String(flags.RdpAddress, "127.0.0.1", "The address of the machine running the RDP server")
	connectCmd.Flags().Int(flags.RdpPort, 3389, "The port to connect to")
	connectCmd.Flags().String(flags.RdpDomain, "", "The domain to connect to")

	if err := viper.BindPFlags(connectCmd.Flags()); err != nil {
		fmt.Println("Could not bind flags.")
	}

	// Username and password are required
	err := connectCmd.MarkFlagRequired(flags.RdpUser)
	if err != nil {
		fmt.Println("Error marking flag (username) as required: ", err)
	}
	err = connectCmd.MarkFlagRequired(flags.RdpPassword)
	if err != nil {
		fmt.Println("Error marking flag (password) as required: ", err)
	}
}
