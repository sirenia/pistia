package cmd

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"sync"
	"time"

	"github.com/kardianos/service"
	"github.com/spf13/viper"
	"gitlab.com/sirenia/pistia/connection"
	"gitlab.com/sirenia/pistia/flags"
	"gitlab.com/sirenia/pistia/kwanza"
	"go.uber.org/zap"

	_ "expvar"
	_ "net/http/pprof"

	_ "gitlab.com/sirenia/pistia/expvars"
)

func NewRunner() *Runner {
	return &Runner{
		connectors:      make(map[string]*connection.Connector),
		connectorsMutex: sync.RWMutex{},
	}
}

type Runner struct {
	cancelFunc      context.CancelFunc
	connectors      map[string]*connection.Connector
	connectorsMutex sync.RWMutex
}

func (r *Runner) Start(s service.Service) error {
	ctx, cancel := context.WithCancel(context.Background())
	r.cancelFunc = cancel

	// Start should not block. Do the actual work async
	go r.run(ctx)

	return nil
}

func (r *Runner) synchronizeConnectors(ctx context.Context, repo *repository, statusUpdates chan<- *kwanza.LoginStatus) {
	r.connectorsMutex.Lock()
	defer r.connectorsMutex.Unlock()
	connectorsToKeep := map[string]struct{}{}
	for _, d := range repo.desktops.All() {
		// Find relevant secrets and converted to plaintext credentials
		for _, login := range d.Descriptive.Logins {

			// Key includes username s.t. we will shut down logins when usernames are added or removed
			connectorKey, err := connection.GenerateKey(d, login, repo.secrets)
			if err != nil {
				zap.L().Warn("Failed to generate connector key, continuing", zap.Error(err), zap.Stringer("desktop", d), zap.Stringer("login", login))
				continue
			}

			if login.Disabled {
				zap.L().Debug("Skipping disabled login, will be stopped if running", zap.Stringer("login", login), zap.String("connectorKey", connectorKey))
				continue
			}

			if c, ok := r.connectors[connectorKey]; !ok {
				newConnector, err := connection.NewConnector(d, login, repo.secrets)
				if err != nil {
					zap.L().Warn("Failed to create connector, continuing", zap.Error(err), zap.Stringer("desktop", d), zap.Stringer("login", login), zap.String("connectorKey", connectorKey))
					continue
				}

				// connectorErrs are errors coming directly from the connector
				connectorErrs := make(chan error, 10)
				// allStatusUpdates are all status updates generated here
				allStatusUpdates := make(chan *kwanza.LoginStatus)
				// debouncedStatusUpdates are all status updates generated here, but debounced
				debounce(viper.GetDuration(flags.DebounceStatusUpdatesInterval), allStatusUpdates, statusUpdates)

				go func(l *kwanza.Login) {
					var status *kwanza.LoginStatus
					if existingStatus, ok := repo.statuses.Find(func(s *kwanza.LoginStatus) bool { return s.Descriptive.For == l.Id }); ok {
						status = existingStatus
					} else {
						// create new status
						status = &kwanza.LoginStatus{
							Descriptive: &kwanza.LoginStatusDescriptive{
								For: l.Id,
							},
						}
					}

				connectorLoop:
					for {
						select {
						case err, ok := <-connectorErrs:
							if !ok {
								zap.L().Info("No more errors from connector", zap.Stringer("login", l))
								break connectorLoop
							}
							zap.L().Debug("Updating status", zap.Stringer("login", l), zap.Error(err), zap.String("connectorKey", connectorKey))

							status.Version = ""
							status.Descriptive.At = time.Now().Unix()
							if err == nil {
								status.Descriptive.Connected = true
								status.Descriptive.Error = ""
							} else {
								status.Descriptive.Connected = false
								status.Descriptive.Error = err.Error()
							}

						case <-time.After(viper.GetDuration(flags.HeartbeatInterval)):
							status.Version = ""
							status.Descriptive.At = time.Now().Unix()
							zap.L().Info("Heartbeat", zap.Stringer("login", l), zap.String("connectorKey", connectorKey), zap.Stringer("status", status), zap.Stringer("desktop", d))
						}

						// Ship status
						allStatusUpdates <- status
					}

					// Signal disconnect (connector is stopped)
					status.Version = ""
					status.Descriptive.At = time.Now().Unix()
					status.Descriptive.Connected = false
					allStatusUpdates <- status

					close(allStatusUpdates)

					zap.L().Debug("Connector loop done", zap.Stringer("login", l), zap.String("connectorKey", connectorKey))
				}(login)

				zap.L().Debug("Starting connector", zap.String("key", newConnector.Key))
				err = newConnector.Start(ctx, connectorErrs)
				if err != nil {
					zap.L().Warn("Failed to start connector", zap.Error(err), zap.Stringer("connector", newConnector), zap.String("connectorKey", newConnector.Key))
					continue
				}
				r.connectors[newConnector.Key] = newConnector
				connectorsToKeep[newConnector.Key] = struct{}{}
			} else {
				connectorsToKeep[c.Key] = struct{}{}
			}
		}
	}

	// Remove and stop connectors that are no longer relevant
	for _, c := range r.connectors {
		_, keepConnector := connectorsToKeep[c.Key]
		if !keepConnector {
			zap.L().Debug("Removing and stopping connector", zap.Stringer("connector", c), zap.String("connectorKey", c.Key))
			err := c.Stop()
			if err != nil {
				zap.L().Warn("Failed to stop connector", zap.Error(err), zap.Stringer("connector", c), zap.String("connectorKey", c.Key))
			}
			delete(r.connectors, c.Key)
			zap.L().Debug("Connector stopped and removed", zap.Stringer("connector", c), zap.String("connectorKey", c.Key))
		}
	}
}

type repository struct {
	desktops *kwanza.Archive[*kwanza.Desktop]
	secrets  *kwanza.Archive[*kwanza.Secret]
	statuses *kwanza.Archive[*kwanza.LoginStatus]
}

// debounce will send the last item received on in to out after period has passed.
func debounce[T any](period time.Duration, in <-chan T, out chan<- T) {
	var lastItem T
	ticker := time.NewTicker(period)
	lastSent := time.Now().Add(-period)
	ready := false
	go func() {
		for {
			select {
			case item, ok := <-in:
				if !ok {
					// we're done
					return
				}
				// Can we send immediately?
				if time.Since(lastSent) > period {
					out <- item
					lastSent = time.Now()
					ticker.Reset(period)
					continue
				}
				// We can't send immediately, so we save the item
				lastItem = item
				ready = true
			case <-ticker.C:
				// We only send if we have a ready item
				if ready {
					out <- lastItem
					lastSent = time.Now()
					ready = false
				}
			}
		}
	}()
}

func (r *Runner) run(ctx context.Context) {
	if viper.GetBool(flags.Debug) {
		go func() {
			zap.L().Info("expvars/pprof available", zap.Int("port", 8081))
			zap.L().Info("expvars/pprof interface done", zap.Error(http.ListenAndServe(fmt.Sprintf(":%v", 8081), http.DefaultServeMux))) // nosemgrep - only on verbose/debug
		}()
	}

	desktops := make(chan []*kwanza.Desktop)
	secrets := make(chan []*kwanza.Secret)
	statuses := make(chan []*kwanza.LoginStatus)
	statusUpdates := make(chan *kwanza.LoginStatus)

	hostname := viper.GetString(flags.Machine)
	machineOnly := viper.GetBool(flags.MachineOnly)

	if machineOnly && hostname == "" {
		// Get actual hostname
		hostnameFromOs, err := os.Hostname()
		if err != nil {
			zap.L().Fatal("Failed to get hostname", zap.Error(err))
		}
		hostname = hostnameFromOs
	}

	if machineOnly {
		zap.L().Info("Running in machine-only mode", zap.String("hostname", hostname))
	}

	// Start continuously getting desktops
	desktopKql := fmt.Sprintf("machine='%s'", hostname)

	go kwanza.Sync(ctx, kwanza.DesktopTypeName, desktopKql, kwanza.DesktopFactory, desktops)
	go kwanza.Sync(ctx, kwanza.SecretTypeName, "", kwanza.SecretFactory, secrets)
	go kwanza.Sync(ctx, kwanza.LoginStatusTypeName, "", kwanza.LoginStatusFactory, statuses)
	go kwanza.Provide(ctx, kwanza.LoginStatusTypeName, statusUpdates)

	// forwards status updates to kwanza
	defer close(statusUpdates)

	// Create archive for secrets, desktops and statuses
	repo := &repository{
		secrets:  kwanza.NewArchive[*kwanza.Secret](),
		desktops: kwanza.NewArchive[*kwanza.Desktop](),
		statuses: kwanza.NewArchive[*kwanza.LoginStatus](),
	}

	statusCond := sync.NewCond(&sync.Mutex{})

	// Keep statuses updated
	go func() {
		for currentStatuses := range statuses {
			zap.L().Debug("Updating statuses", zap.Int("count", len(currentStatuses)))
			repo.statuses.Update(currentStatuses)
			statusCond.Broadcast()
		}
	}()

	// Wait for the first statuses to be received
	statusCond.L.Lock()
	statusCond.Wait()
	statusCond.L.Unlock()

	zap.L().Debug("First statuses received")

	// Keep secrets updated
	go func() {
		for currentSecrets := range secrets {
			zap.L().Debug("Updating secrets", zap.Int("count", len(currentSecrets)))
			repo.secrets.Update(currentSecrets)
			// Also synchronize when secrets are updated
			r.synchronizeConnectors(ctx, repo, statusUpdates)
		}
	}()

	for currentDesktops := range desktops {
		zap.L().Debug("Updating desktops", zap.Int("count", len(currentDesktops)))
		repo.desktops.Update(currentDesktops)
		zap.L().Debug("Updated desktops")
		r.synchronizeConnectors(ctx, repo, statusUpdates)
		zap.L().Debug("Synced connectors (desktop)")
	}
}

func (r *Runner) Stop(s service.Service) error {
	if r.cancelFunc != nil {
		r.cancelFunc()
	}
	// Stop should not block. Return with a few seconds.
	return nil
}
