package cmd

import (
	"context"
	"fmt"
	"os"
	"time"

	"github.com/rs/xid"

	"github.com/kardianos/service"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/sirenia/pistia/about"
	"gitlab.com/sirenia/pistia/aripuana"
	"gitlab.com/sirenia/pistia/flags"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func buildServiceArgs(command string) []string {
	arguments := []string{command}
	for _, flag := range viper.AllKeys() {
		if flag == flags.InstallService || flag == flags.UninstallService || !viper.IsSet(flag) {
			continue
		}
		flagValue := viper.Get(flag)
		switch v := flagValue.(type) {
		case bool:
			if v {
				arguments = append(arguments, fmt.Sprintf("--%v", flag))
			}
		default:
			// Each argument need to be added as a flag and a value
			arguments = append(arguments, fmt.Sprintf("--%v", flag))
			arguments = append(arguments, fmt.Sprintf("%v", flagValue))
		}

	}
	return arguments
}

func banner(version string) string {
	templ := `
                                                             
                                                             
                                                             
           dBBBBBb  dBP.dBBBBP  dBBBBBBP dBP dBBBBBb         
               dB'     BP                         BB         
           dBBBP' dBP  'BBBBb    dBP   dBP    dBP BB         
          dBP    dBP      dBP   dBP   dBP    dBP  BB         
         dBP    dBP  dBBBBP'   dBP   dBP    dBBBBBBB         
         %v                                                   
                                                             
                                                             
  `
	return fmt.Sprintf(templ, version)
}

// runCmd represents the run command
var runCmd = &cobra.Command{
	Use:   "run",
	Short: "Run pistia - maintains configured RDP connections",
	Long:  banner(about.Version),
	Run: func(_ *cobra.Command, _ []string) {
		// Setup zap logger
		var logger *zap.Logger
		if viper.GetBool(flags.Verbose) {
			logger, _ = zap.NewDevelopment()
		} else {
			aripuanaUrl := viper.GetString(flags.AripuanaUrl)
			level, err := zapcore.ParseLevel(viper.GetString(flags.LogLevel))
			if err != nil {
				fmt.Println("Could not parse log level, defaulting to info")
				level = zapcore.InfoLevel
			}
			infoPriority := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool {
				return lvl >= level
			})
			if aripuanaUrl != "" {
				aripuanaLogger := aripuana.NewLogger(context.Background(), aripuanaUrl)
				encoderConfig := zap.NewProductionEncoderConfig()
				encoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
				jsonEncoder := zapcore.NewJSONEncoder(encoderConfig)
				topicAripuana := zapcore.AddSync(aripuanaLogger)
				core := zapcore.NewTee(
					// zapcore.NewCore(jsonEncoder, zapcore.Lock(os.Stdout), infoPriority),
					zapcore.NewCore(jsonEncoder, topicAripuana, infoPriority),
				)

				// Add product name and version to all log entries
				versionField := zap.String("productVersion", about.Version)
				productNameField := zap.String("productName", "pistia")

				machineName, err := os.Hostname()
				if err != nil {
					machineName = viper.GetString(flags.Machine)
				}
				machineNameField := zap.String("machineName", machineName)

				instanceIdField := zap.String("instanceId", xid.New().String())

				logger = zap.New(core).With(productNameField, versionField, machineNameField, instanceIdField)
			} else {
				logger, _ = zap.NewProduction()
			}
		}

		defer func() {
			_ = logger.Sync()
		}()
		zap.ReplaceGlobals(logger)

		r := NewRunner()

		install := viper.GetBool(flags.InstallService)
		uninstall := viper.GetBool(flags.UninstallService)

		if !service.Interactive() || install || uninstall {
			// Configure the service
			cfg := service.Config{
				Name:        "eu.sirenia.pistia",
				DisplayName: "PISTIA - Sirenia RDP Manager",
				Description: "PISTIA - Maintains configured RDP connections s.t. Manatee has desktops to work in.",
				Arguments:   buildServiceArgs("run"),
			}
			s, err := service.New(r, &cfg)
			if err != nil {
				fmt.Println("Failed to create service", err)
			}

			fmt.Println("Service created", cfg.Arguments)

			if install {
				err = s.Install()
				if err != nil {
					fmt.Println("Failed to install service:", err)
				} else {
					fmt.Println("Service installed")
				}
			} else if uninstall {
				err = s.Uninstall()
				if err != nil {
					fmt.Println("Failed to uninstall service", err)
				}
				fmt.Println("Service uninstalled")
			} else {
				err := s.Run()
				if err != nil {
					fmt.Println("Failed to run service", err)
				}
			}
		} else {
			r.run(context.Background())
		}
	},
}

func init() {
	rootCmd.AddCommand(runCmd)
	runCmd.Flags().Bool(flags.MachineOnly, false, "Only maintain connections to desktops on the same machine as pistia is running")
	runCmd.Flags().String(flags.Machine, "", "Name of machine to use when selecting which desktops to connect to. Default is current machine name (if left blank)")

	runCmd.Flags().String(flags.KwanzaUser, "", "Kwanza username")
	runCmd.Flags().String(flags.KwanzaPassword, "", "Kwanza password")
	runCmd.Flags().String(flags.KwanzaUrl, "", "Kwanza url")
	runCmd.Flags().String(flags.KwanzaToken, "", "Kwanza token")
	runCmd.Flags().Duration(flags.KwanzaReconnectInterval, 5*time.Second, "Interval between reconnect attempts to kwanza")

	runCmd.Flags().String(flags.AripuanaUrl, "", "Aripuana url")
	runCmd.Flags().Duration(flags.AripuanaFlushInterval, 30*time.Second, "Interval between flushes to aripuana")

	runCmd.Flags().Bool(flags.InstallService, false, "Install pistia as a service")
	runCmd.Flags().Bool(flags.UninstallService, false, "Uninstall the pistia service")

	runCmd.Flags().String(flags.DefaultRdpDomain, "", "Default RDP domain to use when no domain is specified in the login")
	runCmd.Flags().Int(flags.DefaultRdpPort, 3389, "Default RDP port to use when no port is specified in the login")
	runCmd.Flags().Duration(flags.RdpReconnectInterval, 60*time.Second, "Interval between reconnect attempts to RDP servers")

	runCmd.Flags().Duration(flags.HeartbeatInterval, 120*time.Second, "Interval between connected-and-alive heartbeats to kwanza")
	runCmd.Flags().Duration(flags.DebounceStatusUpdatesInterval, 10*time.Second, "Interval between status updates to kwanza")

	runCmd.Flags().Bool(flags.Debug, false, "Enable debug interfaces (expvars and pprof)")

	runCmd.MarkFlagsOneRequired(flags.KwanzaUser, flags.KwanzaToken)
	runCmd.MarkFlagsMutuallyExclusive(flags.KwanzaUser, flags.KwanzaToken)
	runCmd.MarkFlagsRequiredTogether(flags.KwanzaUser, flags.KwanzaPassword)
	err := runCmd.MarkFlagRequired(flags.KwanzaUrl)
	if err != nil {
		fmt.Println("Could not mark flag (url) as required.")
	}

	if err := viper.BindPFlags(runCmd.Flags()); err != nil {
		fmt.Println("Could not bind flags.")
	}
}
