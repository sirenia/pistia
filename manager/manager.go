package manager

import (
	"sync"

	"gitlab.com/sirenia/pistia/connection"
)

type Manager struct {
	connections map[string]connection.Connection
	mutex       sync.RWMutex
}

func New() *Manager {
	return &Manager{
		connections: make(map[string]connection.Connection),
		mutex:       sync.RWMutex{},
	}
}

func (m *Manager) Add(id string, c connection.Connection) {
	m.mutex.Lock()
	defer m.mutex.Unlock()
	m.connections[id] = c
}

func (m *Manager) Remove(id string) {
	m.mutex.Lock()
	defer m.mutex.Unlock()
	delete(m.connections, id)
}

func (m *Manager) Get(id string) connection.Connection {
	m.mutex.RLock()
	defer m.mutex.RUnlock()
	return m.connections[id]
}
