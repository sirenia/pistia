/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>

*/
package main

import "gitlab.com/sirenia/pistia/cmd"

func main() {
	cmd.Execute()
}
