package expvars

import (
	"expvar"
	"runtime"
)

var Connectors = expvar.NewInt("Connectors") // reports the total number of connectors currently running

func goroutines() interface{} {
	return runtime.NumGoroutine()
}

func init() {
	expvar.Publish("Goroutines", expvar.Func(goroutines))
}
