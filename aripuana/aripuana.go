package aripuana

import (
	context "context"
	"crypto/tls"
	sync "sync"
	"time"

	"github.com/spf13/viper"
	"gitlab.com/sirenia/pistia/flags"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

//go:generate protoc --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative aripuana.proto

type AripuanaLogger struct {
	client LoggerClient
	buffer *BufferedLogMessage
	mutex  *sync.Mutex
	url    string
}

func NewLogger(ctx context.Context, url string) *AripuanaLogger {
	logger := &AripuanaLogger{
		url:    url,
		buffer: &BufferedLogMessage{Buffer: []*LogMessage{}},
		mutex:  &sync.Mutex{},
	}

	// Flush the buffer every 5 seconds
	go func() {
		for {
			select {
			case <-ctx.Done():
				return
			case <-time.After(viper.GetDuration(flags.AripuanaFlushInterval)):
				logger.flush()
			}
		}
	}()

	return logger
}

func (a *AripuanaLogger) connect() (LoggerClient, error) {
	conn, err := grpc.NewClient(
		a.url,
		// nosemgrep - fine for logging
		grpc.WithTransportCredentials(credentials.NewTLS(&tls.Config{
			InsecureSkipVerify: true,
			MinVersion:         tls.VersionTLS13,
		})),
	)
	if err != nil {
		return nil, err
	}

	return NewLoggerClient(conn), nil
}

// Implement the zapcore.WriteSyncer interface

// Write to a buffer
func (a *AripuanaLogger) Write(p []byte) (n int, err error) {
	a.mutex.Lock()
	defer a.mutex.Unlock()

	// Copy bytes to new buffer
	content := append([]byte{}, p...)

	a.buffer.Buffer = append(a.buffer.Buffer, &LogMessage{Content: content})
	return len(p), nil
}

func (a *AripuanaLogger) flush() error {
	a.mutex.Lock()
	defer a.mutex.Unlock()

	if a.client == nil {
		client, err := a.connect()
		if err != nil {
			return err
		}

		a.client = client
	}

	b := a.buffer
	if b == nil || len(b.Buffer) == 0 {
		return nil
	}
	a.buffer = &BufferedLogMessage{Buffer: []*LogMessage{}}
	_, err := a.client.Log(context.Background(), b)
	if err != nil {
		a.client = nil
	}

	return err
}

// Flush the buffer
func (a *AripuanaLogger) Sync() error {
	return a.flush()
}
