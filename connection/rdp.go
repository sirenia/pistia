package connection

import (
	"fmt"
	"log"
	"net"
	"os"
	"sync"
	"time"

	"github.com/tomatome/grdp/core"
	"github.com/tomatome/grdp/glog"
	"github.com/tomatome/grdp/plugin"
	"github.com/tomatome/grdp/protocol/nla"
	"github.com/tomatome/grdp/protocol/pdu"
	"github.com/tomatome/grdp/protocol/sec"
	"github.com/tomatome/grdp/protocol/t125"
	"github.com/tomatome/grdp/protocol/tpkt"
	"github.com/tomatome/grdp/protocol/x224"
	"gitlab.com/sirenia/pistia/screen"
	"go.uber.org/zap"
)

func init() {
	glog.SetLevel(glog.INFO)
	logger := log.New(os.Stdout, "", 0)
	glog.SetLogger(logger)
}

const (
	screenWidth  = 2048
	screenHeight = 1536
)

type Connection interface {
	Connect() error
	Errors() <-chan error
	Close() error
}

func NewRdp(address string, port int, username, password, domain string) Connection {
	return &rdp{
		address:          address,
		port:             port,
		username:         username,
		password:         password,
		domain:           domain,
		errors:           make(chan error, 100),
		errChannelAccess: sync.Mutex{},
	}
}

type rdp struct {
	errors           chan error
	mcs              *t125.MCSClient
	sec              *sec.Client
	pdu              *pdu.Client
	address          string
	username         string
	password         string
	domain           string
	port             int
	errChannelAccess sync.Mutex
}

// Close the RDP connection.
func (r *rdp) Close() error {
	r.errChannelAccess.Lock()
	defer r.errChannelAccess.Unlock()

	// already closed
	if r.mcs == nil {
		return nil
	}

	zap.L().Debug("RDP closing", zap.String("address", r.address), zap.Int("port", r.port), zap.String("username", r.username), zap.String("domain", r.domain))

	err := r.mcs.Close()
	if err != nil {
		zap.L().Warn("Failed to close MCS", zap.String("address", r.address), zap.Int("port", r.port), zap.Error(err), zap.String("username", r.username), zap.String("domain", r.domain))
	}

	err = r.sec.Close()
	if err != nil {
		zap.L().Warn("Failed to close SEC", zap.String("address", r.address), zap.Int("port", r.port), zap.Error(err), zap.String("username", r.username), zap.String("domain", r.domain))
	}

	// msc, sec and pdu are set to nil
	r.pdu = nil
	r.sec = nil
	r.mcs = nil

	// connection has ended
	close(r.errors)
	return err
}

func (r *rdp) Errors() <-chan error {
	return r.errors
}

func (r *rdp) ForwardError(err error) {
	r.errChannelAccess.Lock()
	defer r.errChannelAccess.Unlock()

	if r.pdu == nil {
		zap.L().Debug("Err channel already closed, no errors forwarded", zap.String("address", r.address), zap.Int("port", r.port), zap.String("username", r.username), zap.String("domain", r.domain))
		return
	}

	r.errors <- err
}

// Connect to the RDP server.
func (r *rdp) Connect() error {
	if r.mcs != nil {
		return fmt.Errorf("already connected")
	}

	zap.L().Debug("RDP connecting", zap.String("address", r.address), zap.Int("port", r.port), zap.String("username", r.username), zap.String("domain", r.domain))
	conn, err := net.DialTimeout("tcp", fmt.Sprintf("%s:%d", r.address, r.port), 3*time.Second)
	if err != nil {
		zap.L().Warn("Failed to dial", zap.String("address", r.address), zap.Int("port", r.port), zap.String("username", r.username), zap.String("domain", r.domain), zap.Error(err))
		return err
	}

	t := tpkt.New(core.NewSocketLayer(conn), nla.NewNTLMv2(r.domain, r.username, r.password))
	x := x224.New(t)
	x.SetRequestedProtocol(x224.PROTOCOL_HYBRID)

	r.mcs = t125.NewMCSClient(x)
	r.sec = sec.NewClient(r.mcs)
	r.pdu = pdu.NewClient(r.sec)
	channels := plugin.NewChannels(r.sec)

	r.mcs.SetClientDesktop(uint16(screenWidth), uint16(screenHeight))

	r.sec.SetUser(r.username)
	r.sec.SetPwd(r.password)
	r.sec.SetDomain(r.domain)

	t.SetFastPathListener(r.sec)
	r.sec.SetFastPathListener(r.pdu)
	r.sec.SetChannelSender(r.mcs)
	channels.SetChannelSender(r.sec)
	r.pdu.SetFastPathSender(t)

	r.pdu.On("ready", func() {
		zap.L().Info("RDP connection ready", zap.String("address", r.address), zap.Int("port", r.port), zap.String("username", r.username), zap.String("domain", r.domain))
		r.ForwardError(nil)
	})

	r.mcs.On("error", func(e error) {
		zap.L().Warn("[mcs] error", zap.Error(e), zap.String("address", r.address), zap.Int("port", r.port), zap.String("username", r.username), zap.String("domain", r.domain))

		err := conn.Close()
		if err != nil {
			zap.L().Warn("Failed to close underlying connection [mcs]", zap.Error(err), zap.String("address", r.address), zap.Int("port", r.port), zap.String("username", r.username), zap.String("domain", r.domain))
		}

		r.ForwardError(e)
	})

	r.sec.On("error", func(e error) {
		zap.L().Warn("[sec] error", zap.Error(e), zap.String("address", r.address), zap.Int("port", r.port), zap.String("username", r.username), zap.String("domain", r.domain))

		err := conn.Close()
		if err != nil {
			zap.L().Warn("Failed to close underlying connection [sec]", zap.Error(err), zap.String("address", r.address), zap.Int("port", r.port), zap.String("username", r.username), zap.String("domain", r.domain))
		}

		r.ForwardError(e)
	})

	r.pdu.On("error", func(e error) {
		// Any errors should force a reconnect
		zap.L().Warn("[pdu] error", zap.Error(e), zap.String("address", r.address), zap.Int("port", r.port), zap.String("username", r.username), zap.String("domain", r.domain))

		err := conn.Close()
		if err != nil {
			zap.L().Warn("Failed to close underlying connection [pdu]", zap.Error(err), zap.String("address", r.address), zap.Int("port", r.port), zap.String("username", r.username), zap.String("domain", r.domain))
		}

		r.ForwardError(e)
	})

	scr := &screen.Screen{Width: screenWidth, Height: screenHeight}
	r.pdu.On("bitmap", scr.Patch)

	// g.x224.SetRequestedProtocol(x224.PROTOCOL_RDP)
	// g.x224.SetRequestedProtocol(x224.PROTOCOL_SSL)

	err = x.Connect()
	if err != nil {
		zap.L().Warn("Connection failed (x224)", zap.Error(err), zap.String("address", r.address), zap.Int("port", r.port), zap.String("username", r.username), zap.String("domain", r.domain))
		closeConnErr := conn.Close()
		if closeConnErr != nil {
			zap.L().Warn("Failed to close underlying connection [x224]", zap.Error(err), zap.String("address", r.address), zap.Int("port", r.port), zap.String("username", r.username), zap.String("domain", r.domain))
		}
		return err
	}
	zap.L().Info("Connected", zap.String("address", r.address), zap.Int("port", r.port), zap.String("username", r.username), zap.String("domain", r.domain))

	return nil
}
