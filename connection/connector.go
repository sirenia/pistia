package connection

import (
	"context"
	"crypto/md5"
	"fmt"
	"time"

	"github.com/spf13/viper"
	"gitlab.com/sirenia/pistia/expvars"
	"gitlab.com/sirenia/pistia/flags"
	"gitlab.com/sirenia/pistia/kwanza"
	"go.uber.org/zap"
)

var (
	errDesktopNil    = fmt.Errorf("desktop is nil")
	errLoginNil      = fmt.Errorf("login is nil")
	errNoSecrets     = fmt.Errorf("secrets is nil")
	errNoSecretFound = fmt.Errorf("no secret found")
	errNoUserFound   = fmt.Errorf("no user found")
	errNoHost        = fmt.Errorf("no host found")
	errNoConnection  = fmt.Errorf("no connection available")
)

// Connector is a connection to a remote desktop.
type Connector struct {
	conn     Connection
	Desktop  *kwanza.Desktop
	Login    *kwanza.Login
	secrets  *kwanza.Archive[*kwanza.Secret]
	Key      string
	deadness context.Context
	die      context.CancelFunc
}

func NewConnector(desktop *kwanza.Desktop, login *kwanza.Login, secrets *kwanza.Archive[*kwanza.Secret]) (*Connector, error) {
	key, err := GenerateKey(desktop, login, secrets)
	if err != nil {
		zap.L().Warn("Failed to generate key for connector", zap.Error(err), zap.Stringer("desktop", desktop), zap.Stringer("login", login))
		return nil, err
	}

	return &Connector{
		Key:     key,
		Desktop: desktop,
		Login:   login,
		secrets: secrets,
	}, nil
}

func (c *Connector) String() string {
	return fmt.Sprintf("Connector{Desktop: %s, Login: %s, Key: %s}", c.Desktop, c.Login, c.Key)
}

func GenerateKey(desktop *kwanza.Desktop, login *kwanza.Login, secrets *kwanza.Archive[*kwanza.Secret]) (string, error) {
	if desktop == nil {
		return "", errDesktopNil
	}

	if login == nil || (login.UserSecret == nil && login.UserPlaintext == "") {
		return "", errLoginNil
	}

	if secrets == nil {
		return "", errNoSecrets
	}

	userKey := login.UserPlaintext

	if userKey == "" {

		// Lookup secret for login (username)
		secret, ok := secrets.Get(login.UserSecret.Secret)
		if !ok || secret == nil {
			return "", errNoSecretFound
		}

		encryptedUserProperty, ok := secret.Descriptive.Encrypted[login.UserSecret.Property]
		if !ok {
			return "", errNoUserFound
		}

		// Secret "key" is encrypted value for property concatenated with type and validUntil
		secretKey := fmt.Sprintf("%s-%s-%d", encryptedUserProperty, secret.Descriptive.Type, secret.Descriptive.ValidUntil)
		userKey = secretKey
	}

	// nosemgrep - not used for crypto
	h := md5.New()
	fmt.Fprintf(h, "%s-%s-%s", desktop.Key(), login.Key(), userKey)

	return fmt.Sprintf("%x", h.Sum(nil)), nil
}

func (c *Connector) Identifier() string {
	return c.Key
}

func findPort(desktop *kwanza.Desktop, login *kwanza.Login) int {
	port := login.Port
	if port == nil || *port == 0 {
		port = desktop.Descriptive.Port
	}
	viperPort := viper.GetInt(flags.DefaultRdpPort)
	if port == nil || *port == 0 {
		port = &viperPort
	}

	defaultPort := 3389
	if port == nil || *port == 0 {
		port = &defaultPort // default rdp port
	}

	return *port
}

func findHost(desktop *kwanza.Desktop, login *kwanza.Login) (string, error) {
	host := login.Host
	if host == "" {
		host = desktop.Descriptive.Host
	}

	if host == "" {
		return "", errNoHost
	}
	return host, nil
}

func findDomain(login *kwanza.Login) string {
	domain := login.Domain
	if domain == "" {
		domain = viper.GetString(flags.DefaultRdpDomain)
	}
	return domain
}

func connect(address string, port int, username, password, domain string) (<-chan error, Connection, error) {
	conn := NewRdp(address, port, username, password, domain)
	return conn.Errors(), conn, conn.Connect()
}

func exposeSecret(secret, property string, secrets *kwanza.Archive[*kwanza.Secret]) (string, error) {
	s, ok := secrets.Get(secret)
	if !ok || s == nil {
		zap.L().Warn("No secret found", zap.String("secret", secret), zap.String("property", property))
		return "", errNoSecretFound
	}
	plaintext, err := s.DecryptProperty(property)
	if err != nil {
		zap.L().Warn("Failed to decrypt from secret", zap.Error(err), zap.String("secret", secret), zap.String("property", property))
		return "", err
	}

	return plaintext, nil
}

// awaitDemiseOrTimeout waits for death or timeout whichever comes first. Returns true if death came first.
func awaitDemiseOrTimeout(ctx context.Context) bool {
	// Are we already dead?
	if ctx.Err() != nil {
		return true
	}

	// If not we might have to wait for a timeout
	select {
	case <-time.After(viper.GetDuration(flags.RdpReconnectInterval)):
		return false
	case <-ctx.Done():
		return true
	}
}

func (c *Connector) isDead() bool {
	return c.deadness != nil && c.deadness.Err() != nil
}

func (c *Connector) Start(ctx context.Context, errors chan<- error) error {
	if c.deadness != nil {
		zap.L().Warn("Connector is already started or long dead and cannot be restarted", zap.Stringer("desktop", c.Desktop), zap.Stringer("login", c.Login))
		return errNoConnection
	}

	zap.L().Info("Starting connector", zap.Stringer("desktop", c.Desktop), zap.Stringer("login", c.Login))

	// First we need connection info
	host, err := findHost(c.Desktop, c.Login)
	if err != nil {
		zap.L().Warn("No host to connect to", zap.Error(err), zap.Stringer("desktop", c.Desktop), zap.Stringer("login", c.Login))
		errors <- err
		return err
	}
	port := findPort(c.Desktop, c.Login)
	domain := findDomain(c.Login)

	// Deadness
	deadness, die := context.WithCancel(ctx)
	c.deadness = deadness
	c.die = die

	// Then we'll start a reconnect loop that will try to connect to the remote desktop.
	// Each iteration will take username and password from the secrets archive and try to connect.
	// If the connection fails, it will try again a few seconds later
	go func() {
		expvars.Connectors.Add(1)
		defer func() {
			if !c.isDead() {
				c.die()
			}
			close(errors)
			expvars.Connectors.Add(-1)
		}()

		for {
			// Check if we're dead to start with
			if c.isDead() {
				zap.L().Info("Connector stopped, ending loop", zap.Stringer("desktop", c.Desktop), zap.Stringer("login", c.Login))
				return
			}

			// username might be plaintext
			username := c.Login.UserPlaintext

			if username == "" {
				// Decrypt username and password
				username, err = exposeSecret(c.Login.UserSecret.Secret, c.Login.UserSecret.Property, c.secrets)
				if err != nil {
					errors <- err
					zap.L().Warn("Failed to decrypt username, retrying", zap.Error(err), zap.Stringer("desktop", c.Desktop), zap.Stringer("login", c.Login))
					if awaitDemiseOrTimeout(deadness) {
						return
					}
					continue
				}
			}

			password, err := exposeSecret(c.Login.PasswordSecret.Secret, c.Login.PasswordSecret.Property, c.secrets)
			if err != nil {
				errors <- err
				zap.L().Warn("Failed to decrypt password, retrying", zap.Error(err), zap.Stringer("desktop", c.Desktop), zap.Stringer("login", c.Login))
				if awaitDemiseOrTimeout(deadness) {
					return
				}
				continue
			}

			connectionErrs, conn, err := connect(host, port, username, password, domain)
			// save the connection so we can stop it later
			c.conn = conn

			if err != nil {
				errors <- err
				// Retry on all errors
				zap.L().Info("Retrying initial connection", zap.Error(err), zap.Stringer("desktop", c.Desktop), zap.Stringer("login", c.Login))
				if awaitDemiseOrTimeout(deadness) {
					return
				}
				continue
			}

			// connected
			zap.L().Info("Connected to remote desktop, not necessarily logged in ... waiting", zap.Stringer("desktop", c.Desktop), zap.Stringer("login", c.Login))
			err, ok := <-connectionErrs
			// Always forward
			errors <- err

			// no more errors to forward
			if !ok {
				zap.L().Warn("Connection closed, no more errors to forward", zap.Stringer("desktop", c.Desktop), zap.Stringer("login", c.Login))
				return
			}

			// if there was an error, we retry
			if err != nil {
				zap.L().Warn("Connection error, retrying", zap.Error(err), zap.Stringer("desktop", c.Desktop), zap.Stringer("login", c.Login))
				if awaitDemiseOrTimeout(deadness) {
					return
				}
				continue
			}

			// if not we wait for any errors
			select {
			case <-ctx.Done():
				err := c.Stop()
				if err != nil {
					zap.L().Warn("Failed to stop connector", zap.Error(err), zap.Stringer("desktop", c.Desktop), zap.Stringer("login", c.Login))
				}
				return
			case err, ok := <-connectionErrs:
				// We retry on all errors
				if err != nil && ok {
					errors <- err
					if awaitDemiseOrTimeout(ctx) {
						return
					}
					zap.L().Info("Retrying connection", zap.Error(err), zap.Stringer("desktop", c.Desktop), zap.Stringer("login", c.Login))
					continue
				}
				// !ok means connection was closed
				zap.L().Info("Connection loop ending", zap.Stringer("desktop", c.Desktop), zap.Stringer("login", c.Login))
				return
			}
		}
	}()

	return nil
}

func (c *Connector) Stop() error {
	if c.deadness == nil {
		zap.L().Warn("Connector not started", zap.Stringer("desktop", c.Desktop), zap.Stringer("login", c.Login))
		return nil
	}
	if c.deadness.Err() != nil {
		zap.L().Warn("Connector already stopped", zap.Stringer("desktop", c.Desktop), zap.Stringer("login", c.Login))
		return nil
	}
	if c.die == nil {
		zap.L().Warn("No way to stop connector", zap.Stringer("desktop", c.Desktop), zap.Stringer("login", c.Login))
		return fmt.Errorf("no way to stop connector %s", c.Key)
	}
	c.die()
	zap.L().Info("Stopping connector, context cancelled", zap.Stringer("desktop", c.Desktop), zap.Stringer("login", c.Login))
	if c.conn != nil {
		zap.L().Info("Stopping connector", zap.Stringer("desktop", c.Desktop), zap.Stringer("login", c.Login))
		return c.conn.Close()
	}
	zap.L().Warn("No connection to close", zap.Stringer("desktop", c.Desktop), zap.Stringer("login", c.Login))
	return errNoConnection
}
