package connection

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/sirenia/pistia/kwanza"
)

func TestGenerateKey(t *testing.T) {
	_, err := GenerateKey(nil, nil, nil)
	assert.Error(t, err)

	defaultPort := 1234

	desktop := &kwanza.Desktop{
		Id:      "desktop-id",
		Name:    "desktop-name",
		Version: "desktop-version",
		Descriptive: &kwanza.DesktopDescriptive{
			Host:        "desktop-host",
			Port:        &defaultPort,
			Machine:     "desktop-machine",
			ConnectWith: "desktop-connect-with",
			Logins:      []*kwanza.Login{},
		},
	}
	_, err = GenerateKey(desktop, nil, nil)
	assert.Error(t, err)

	login := &kwanza.Login{
		UserSecret:     &kwanza.SecretReference{Secret: "user-secret", Property: "user-property"},
		PasswordSecret: &kwanza.SecretReference{Secret: "password-secret", Property: "password-property"},
		Domain:         "login-domain",
		Host:           "login-host",
		Port:           &defaultPort,
	}
	_, err = GenerateKey(desktop, login, nil)
	assert.Error(t, err)

	secrets := kwanza.NewArchive[*kwanza.Secret]()

	// no matching user secret
	_, err = GenerateKey(desktop, login, secrets)
	assert.Error(t, err)

	// add matching secret
	secret := &kwanza.Secret{
		Id:      "user-secret",
		Name:    "user-secret-name",
		Version: "user-secret-version",
		Descriptive: &kwanza.SecretDescriptive{
			Type:       "user-secret-type",
			ValidUntil: 1234,
			Encrypted: map[string]string{
				"user-property":     "user-property-value",
				"password-property": "password-property-value",
			},
		},
	}
	secrets.Add(secret)

	key, err := GenerateKey(desktop, login, secrets)
	assert.NoError(t, err)
	assert.NotEmpty(t, key)

	// changing name, or version of the desktop should change not change key
	desktop.Name = "desktop-name2"
	desktop.Version = "desktop-version2"

	key2, err := GenerateKey(desktop, login, secrets)
	assert.NoError(t, err)
	assert.Equal(t, key, key2)

	// changing id, host, port, machine, or connectWith of the desktop should change key
	// - Id
	desktop.Id = "desktop-id2"
	key2, err = GenerateKey(desktop, login, secrets)
	assert.NoError(t, err)
	assert.NotEqual(t, key, key2)

	// - Host
	desktop.Id = "desktop-id"
	desktop.Descriptive.Host = "desktop-host2"
	key3, err := GenerateKey(desktop, login, secrets)
	assert.NoError(t, err)
	assert.NotEqual(t, key, key3)

	newPort := 1235
	// - Port
	desktop.Descriptive.Host = "desktop-host"
	desktop.Descriptive.Port = &newPort
	key4, err := GenerateKey(desktop, login, secrets)
	assert.NoError(t, err)
	assert.NotEqual(t, key, key4)

	// - Machine
	desktop.Descriptive.Port = &defaultPort
	desktop.Descriptive.Machine = "desktop-machine2"
	key5, err := GenerateKey(desktop, login, secrets)
	assert.NoError(t, err)
	assert.NotEqual(t, key, key5)

	// - ConnectWith
	desktop.Descriptive.Machine = "desktop-machine"
	desktop.Descriptive.ConnectWith = "desktop-connect-with2"
	key6, err := GenerateKey(desktop, login, secrets)
	assert.NoError(t, err)
	assert.NotEqual(t, key, key6)

	// reset ConnectWith
	desktop.Descriptive.ConnectWith = "desktop-connect-with"

	// Changing usersecret, passwordsecret, domain, host, or port of the login should change key

	// Add another secret (user)
	secret2 := &kwanza.Secret{
		Id:      "user-secret2",
		Name:    "user-secret-name2",
		Version: "user-secret-version2",
		Descriptive: &kwanza.SecretDescriptive{
			Type:       "user-secret-type2",
			ValidUntil: 1234,
			Encrypted: map[string]string{
				"user-property":     "user-property-value2",
				"password-property": "password-property-value2",
			},
		},
	}
	secrets.Add(secret2)

	login.UserSecret.Secret = "user-secret2"
	key7, err := GenerateKey(desktop, login, secrets)
	assert.NoError(t, err)
	assert.NotEqual(t, key, key7)

	// same also applies if we choose another property
	login.UserSecret.Secret = "user-secret"
	login.UserSecret.Property = "password-property"

	key8, err := GenerateKey(desktop, login, secrets)
	assert.NoError(t, err)
	assert.NotEqual(t, key, key8)

	// Add another secret (password)
	secret3 := &kwanza.Secret{
		Id:      "password-secret2",
		Name:    "password-secret-name2",
		Version: "password-secret-version2",
		Descriptive: &kwanza.SecretDescriptive{
			Type:       "password-secret-type2",
			ValidUntil: 1234,
			Encrypted: map[string]string{
				"user-property":     "user-property-value2",
				"password-property": "password-property-value2",
			},
		},
	}
	secrets.Add(secret3)

	login.UserSecret.Property = "user-property"
	login.PasswordSecret.Secret = "password-secret2"

	key9, err := GenerateKey(desktop, login, secrets)
	assert.NoError(t, err)
	assert.NotEqual(t, key, key9)

	// same also applies if we choose another property
	login.PasswordSecret.Secret = "password-secret"
	login.PasswordSecret.Property = "user-property"

	key10, err := GenerateKey(desktop, login, secrets)
	assert.NoError(t, err)
	assert.NotEqual(t, key, key10)

	login.PasswordSecret.Property = "password-property"

	// - domain
	login.Domain = "login-domain2"
	key11, err := GenerateKey(desktop, login, secrets)
	assert.NoError(t, err)
	assert.NotEqual(t, key, key11)

	// - port
	login.Domain = "login-domain"
	login.Port = &newPort
	key12, err := GenerateKey(desktop, login, secrets)
	assert.NoError(t, err)
	assert.NotEqual(t, key, key12)

	// - host
	login.Port = &defaultPort
	login.Host = "login-host2"
	key13, err := GenerateKey(desktop, login, secrets)
	assert.NoError(t, err)
	assert.NotEqual(t, key, key13)

	// change user-name of secret should change key
	login.Host = "login-host"
	secret.Descriptive.Encrypted["user-property"] = "user-property-value2"

	key15, err := GenerateKey(desktop, login, secrets)
	assert.NoError(t, err)
	assert.NotEqual(t, key, key15)

	// change password-name of secret should NOT change key
	secret.Descriptive.Encrypted["user-property"] = "user-property-value"
	secret.Descriptive.Encrypted["password-property"] = "password-property-value2"

	key16, err := GenerateKey(desktop, login, secrets)
	assert.NoError(t, err)
	assert.Equal(t, key, key16)

	// change disabled property of login should change key
	login.Disabled = true
	key17, err := GenerateKey(desktop, login, secrets)
	assert.NoError(t, err)
	assert.NotEqual(t, key, key17)

	// plaintext username should change key
	login.Disabled = false
	login.UserPlaintext = "user-plaintext"
	key18, err := GenerateKey(desktop, login, secrets)
	assert.NoError(t, err)
	assert.NotEqual(t, key, key18)
}
