package connection

// Credentials are the credentials used to connect to a remote desktop.
// All fields are plaintext.
type Credentials struct {
	Username string
	Password string
	Domain   string
}

func (c *Credentials) Equals(other *Credentials) bool {
	return c.Username == other.Username &&
		c.Password == other.Password &&
		c.Domain == other.Domain
}
